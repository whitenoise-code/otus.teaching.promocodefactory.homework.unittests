﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class PartnersService: IPartnersService
    {
        private readonly IRepository<Partner> _partnersRepository;
        private readonly ICurrentDateTimeProvider _dateTimeProvider;

        public PartnersService(IRepository<Partner> partnersRepository, ICurrentDateTimeProvider dateTimeProvider)
        {
            _partnersRepository = partnersRepository;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<Partner> GetPartnerAsync(Guid partnerId)
        {
            return await _partnersRepository.GetByIdAsync(partnerId);
        }

        public async Task<Guid> SetLimitToPartnerAsync(Partner partner, SetPartnerPromoCodeLimitRequest request)
        {
            if (request.Limit <= 0)
                throw new PartnersDomainException("Лимит должен быть больше 0");

            if (request.EndDate < _dateTimeProvider.GetCurrentTime())
                throw new PartnersDomainException("Дата истечения срока действия промокода должна быть позже текущего времени");

            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new PartnersDomainException("Данный партнер не активен");

            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                x.CancelDate is null);

            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = _dateTimeProvider.GetCurrentTime();
            }

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = _dateTimeProvider.GetCurrentTime(),
                EndDate = request.EndDate
            };

            partner.PartnerLimits.Add(newLimit);

            await _partnersRepository.UpdateAsync(partner);

            return newLimit.Id;
        }
    }
}
