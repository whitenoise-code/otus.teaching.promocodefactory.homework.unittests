﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public interface IPartnersService
    {
        Task<Partner> GetPartnerAsync(Guid partnerId);

        Task<Guid> SetLimitToPartnerAsync(Partner partner, SetPartnerPromoCodeLimitRequest request);
    }
}
