﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Exceptions
{
    public class PartnersDomainException: Exception
    {
        public PartnersDomainException()
        { }

        public PartnersDomainException(string message)
            : base(message)
        { }

        public PartnersDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
