﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Services
{
    public class PartnersServiceTests
    {
        [Theory, AutoMoqData]
        public async void GetPartnerAsync_PartnerNotFound_ReturnsNull(
            [Frozen] Mock<IRepository<Partner>> repoMock,
            [Frozen] PartnersService sut)
        {
            var partnerId = Guid.NewGuid();
            repoMock.Setup(repository => repository.GetByIdAsync(partnerId))
                .ReturnsAsync(default(Partner));

            var result = await sut.GetPartnerAsync(partnerId);

            result.Should().BeNull();
        }

        [Theory, AutoMoqData]
        public void SetLimitToPartnerAsync_PartnerIsInactive_ThrowsException(
            [Frozen] Mock<IRepository<Partner>> repoMock,
            [Frozen] PartnersService sut)
        {
            var guid = Guid.Parse("db61c648-e528-4b1a-8df5-44d2c5c2a2e4");
            var partner = new PartnerBuilder(guid)
                .IsActive(false)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(5)
                .EndsOn(DateTime.MaxValue)
                .Build();

            Func<Task> act = async () => await sut.SetLimitToPartnerAsync(partner, request);

            act.Should().Throw<PartnersDomainException>()
                .WithMessage("Данный партнер не активен");

        }

        [Theory, AutoMoqData]
        public void SetLimitToPartnerAsync_LimitIsNonPositive_ThrowsException(
            [Frozen] PartnersService sut)
        {
            var guid = Guid.Parse("db61c648-e528-4b1a-8df5-44d2c5c2a2e4");
            var partner = new PartnerBuilder(guid)
                .IsActive()
                .Build();
            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(0)
                .Build();

            Func<Task> act = async () => await sut.SetLimitToPartnerAsync(partner, request);

            act.Should().Throw<PartnersDomainException>()
                .WithMessage("Лимит должен быть больше 0");
        }

        [Theory, AutoMoqData]
        public void SetLimitToPartnerAsync_IncorrectPromocodeEndDate_ThrowsException(
            [Frozen] Mock<ICurrentDateTimeProvider> currentDateTimeProviderMock,
            [Frozen] PartnersService sut)
        {
            var guid = Guid.Parse("db61c648-e528-4b1a-8df5-44d2c5c2a2e4");
            var partner = new PartnerBuilder(guid)
                .IsActive()
                .Build();
            
            var currentTime = DateTime.Now;
            currentDateTimeProviderMock
                .Setup(provider => provider.GetCurrentTime())
                .Returns(currentTime);
            
            var endDate = DateTime.MinValue;
            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(10)
                .EndsOn(endDate)
                .Build();

            Func<Task> act = async () => await sut.SetLimitToPartnerAsync(partner, request);

            act.Should().Throw<PartnersDomainException>()
                .WithMessage("Дата истечения срока действия промокода должна быть позже текущего времени");
        }

        [Theory, AutoMoqData]
        public async void SetLimitToPartnerAsync_PreviousLimitIsActive_ClosePreviousLimit(
            [Frozen] Mock<IRepository<Partner>> repoMock,
            [Frozen] Mock<ICurrentDateTimeProvider> currentDateTimeProviderMock,
            [Frozen] PartnersService sut)
        {

            var guid = Guid.Parse("db61c648-e528-4b1a-8df5-44d2c5c2a2e4");
            var partner = new PartnerBuilder(guid)
                .IsActive()
                .With(new PartnerLimitBuilder().LimitedBy(1).Build())
                .WithPromocodes(10)
                .Build();
            var limitCount = 3;
            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(limitCount)
                .Build();

            repoMock
                .Setup(repository => repository.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var currentTime = DateTime.Now;
            currentDateTimeProviderMock
                .Setup(provider => provider.GetCurrentTime())
                .Returns(currentTime);

            var result = await sut.SetLimitToPartnerAsync(partner, request);

            partner.PartnerLimits.First().CancelDate.Should().Be(currentTime);
            partner.PartnerLimits.Count.Should().Be(2);

        }

        [Theory, AutoMoqData]
        public async void SetLimitToPartnerAsync_PreviousLimitIsActive_ResetPromocodesNumber(
            [Frozen] Mock<IRepository<Partner>> repoMock,
            [Frozen] PartnersService sut)
        {
            var partnerId = Guid.Parse("db61c648-e528-4b1a-8df5-44d2c5c2a2e4");
            var partner = new PartnerBuilder(partnerId)
                .With(new PartnerLimitBuilder().LimitedBy(1).Build())
                .WithPromocodes(10)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(2)
                .Build();

            repoMock.Setup(repository => repository.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            await sut.SetLimitToPartnerAsync(partner, request);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Theory, AutoMoqData]
        public async void SetLimitToPartnerAsync_PreviousLimitExpired_KeepPromocodesNumber(
            [Frozen] Mock<IRepository<Partner>> repoMock,
            [Frozen] PartnersService sut)
        {
            var partnerId = Guid.Parse("db61c648-e528-4b1a-8df5-44d2c5c2a2e4");
            var partner = new PartnerBuilder(partnerId)
                .With(new PartnerLimitBuilder()
                        .CanceledAt(DateTime.MaxValue)
                        .Build())
                .WithPromocodes(10)
                .Build();
            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(2)
                .Build();
            repoMock.Setup(repository => repository.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            await sut.SetLimitToPartnerAsync(partner, request);

            partner.NumberIssuedPromoCodes.Should().Be(partner.NumberIssuedPromoCodes);

        }

        [Theory, AutoMoqData]
        public async void SetLimitToPartnerAsync_CorrectLimit_CallsUpdatePartner(
            [Frozen] Mock<IRepository<Partner>> repoMock,
            [Frozen] PartnersService sut)
        {
            var partnerId = Guid.Parse("db61c648-e528-4b1a-8df5-44d2c5c2a2e4");
            var partner = new PartnerBuilder(partnerId)
                .With(new PartnerLimitBuilder().LimitedBy(1).Build())
                .WithPromocodes(10)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(2)
                .Build();

            await sut.SetLimitToPartnerAsync(partner, request);

            repoMock.Verify(repository => repository.UpdateAsync(partner), Times.Once);
        }
    }
}
