﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {

        [Theory, AutoMoqData]
        public async void SetPartnerPromoCodeLimit_PartnerNotFound_Returns404Error(
            [Frozen] Mock<IPartnersService> partnersServiceMock,
            [Frozen, NoAutoProperties] PartnersController sut)
        {
            var partnerId = Guid.NewGuid();
            partnersServiceMock.Setup(service => service.GetPartnerAsync(partnerId))
                .ReturnsAsync(default(Partner));

            var result =
                await sut.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest() {Limit = 2});

            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory, AutoMoqData]
        public async void SetPartnerPromoCodeLimit_PartnerServiceThrowsException_ReturnsBadRequestObjectResult(
            [Frozen] Mock<IPartnersService> partnersServiceMock,
            [Frozen, NoAutoProperties] PartnersController sut)
        {
            var guid = Guid.Parse("db61c648-e528-4b1a-8df5-44d2c5c2a2e4");
            var partner = new PartnerBuilder(guid)
                .IsActive(false)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(5)
                .EndsOn(DateTime.MaxValue)
                .Build();

            partnersServiceMock
                .Setup(service =>
                    service.SetLimitToPartnerAsync(It.IsNotNull<Partner>(), It.IsNotNull<SetPartnerPromoCodeLimitRequest>()))
                .ThrowsAsync(new PartnersDomainException());

            var result =
                await sut.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}
