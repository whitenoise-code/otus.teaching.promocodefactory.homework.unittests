﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    class PartnerLimitBuilder
    {
        private readonly PartnerPromoCodeLimit _limit;

        public PartnerLimitBuilder()
        {
            _limit = new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("91a2846f-a9fb-443f-b3bd-30e621ab7986"),
                CreateDate = DateTime.MinValue,
                EndDate = DateTime.MaxValue,
                CancelDate = null,
                Limit = 5,

            };
        }

        public PartnerLimitBuilder LimitedBy(int limit)
        {
            _limit.Limit = limit;
            return this;
        }

        public PartnerLimitBuilder PartnerId(Guid partnerId)
        {
            _limit.PartnerId = partnerId;
            return this;
        }

        public PartnerLimitBuilder CanceledAt(DateTime cancelDate)
        {
            _limit.CancelDate = cancelDate;
            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return _limit;
        }
    }
}
