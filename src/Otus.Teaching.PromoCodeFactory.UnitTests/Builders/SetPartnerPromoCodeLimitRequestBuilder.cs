﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    class SetPartnerPromoCodeLimitRequestBuilder
    {
        private SetPartnerPromoCodeLimitRequest _request;

        public SetPartnerPromoCodeLimitRequestBuilder()
        {
            _request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.MaxValue, Limit = 2 };
        }

        public SetPartnerPromoCodeLimitRequestBuilder EndsOn(DateTime endDate)
        {
            _request.EndDate = endDate;
            return this;
        }

        public SetPartnerPromoCodeLimitRequestBuilder WithLimit(int limit)
        {
            _request.Limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return _request;
        }
    }
}
