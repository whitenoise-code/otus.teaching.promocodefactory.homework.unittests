﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    class PartnerBuilder
    {
        private readonly Partner _partner;
        
        public PartnerBuilder(Guid guid)
        {
            _partner = new Partner()
            {
                Id = guid,
                IsActive = true,
                Name = "TestPartnerName",
                NumberIssuedPromoCodes = 10,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
        }
        
        public PartnerBuilder IsActive(bool isActive = true)
        {
            _partner.IsActive = isActive;
            return this;
        }

        public PartnerBuilder With(string name)
        {
            _partner.Name = name;
            return this;
        }

        public PartnerBuilder With(PartnerPromoCodeLimit promocodeLimit)
        {
            promocodeLimit.PartnerId = _partner.Id;
            _partner.PartnerLimits.Add(promocodeLimit);
            return this;
        }

        public PartnerBuilder WithPromocodes(int numberIssuedPromocodes)
        {
            _partner.NumberIssuedPromoCodes = numberIssuedPromocodes;
            return this;
        }

        public Partner Build()
        {
            return _partner;
        }
    }
}
